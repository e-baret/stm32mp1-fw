#include "openamp.h"
#include "virt_uart.h"
#include "openamp_log.h"
#include "rpmsg_hdr.h"

#include "user_openamp.h"

#define SDB_BUFFER_SIZE 512

extern struct rpmsg_virtio_device rvdev;
extern RPMSG_HDR_HandleTypeDef hsdb0;

extern __IO FlagStatus SDB0RxMsg;
extern uint8_t SDB0ChannelBuffRx[SDB_BUFFER_SIZE];
extern uint8_t SDB0ChannelBuffTx[SDB_BUFFER_SIZE];
extern uint16_t SDB0ChannelRxSize;



void User_HDR_Init(void);

void SDB0_RxCpltCallback(RPMSG_HDR_HandleTypeDef *huart);

int User_SDB_write(uint8_t bufferid, uint8_t* msg, uint32_t size);
struct DDR_readBuffer User_SDB0_read(char* buffer);
int User_SDB0_echo(void);
int User_SDB0_bandwidth(void);
int User_SDB0_bandwidth_v2(void);

void treatSDBEvent();

int get_DDR_isInit(void);
void set_DDR_isInit(int state);
void set_DDR_bufferToInit(int state);
void set_DDR_readyToInit(int state);

