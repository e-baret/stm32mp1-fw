#include "user_rpmsgDDR.h"
#include "user_openamp.h"

RPMSG_HDR_HandleTypeDef hsdb0;
struct rpmsg_virtio_device rvdev;

__IO FlagStatus SDB0RxMsg = RESET;
uint8_t SDB0ChannelBuffRx[SDB_BUFFER_SIZE]={'\0'};
uint8_t SDB0ChannelBuffTx[SDB_BUFFER_SIZE]={'\0'};
uint16_t SDB0ChannelRxSize = 0;

////////////////////////////////////////////////////////////////////
#define MAX_DDR_BUFF 16

#define SAMP_SRAM_RPMSG_PACKET_SIZE 	(256*2)
#define SAMP_SRAM_DDR_DMA_PACKET_SIZE 	(1024*2)
#define SAMP_DDR_BUFFER_SIZE 		(1024*1024)
#define SAMP_PERIPH_2_DDR_BUFFER_SIZE (32*1024)


static int DDR_isInit = 0;
static int DDR_readyToInit = 0;
static int DDR_nbInitBuffer = 0;
static int DDR_bufferToInit = 0;

typedef struct DDR_SharedBuffer{
	uint8_t B;
	uint8_t id; // decimal format
	uint8_t A;
	uint32_t physAddr; // 8-digit hexadecimal format
	uint8_t L;
	uint32_t physSize; // 8-digit hexadecimal format
};

typedef struct DDR_readBuffer{
	uint8_t id; // decimal format
	uint32_t size; // 8-digit hexadecimal format
};

struct DDR_SharedBuffer sharedMemBuff[10]; // used to store DDR buff allocated by Linux driver

char mSdbBuffTx[512];
//#define TRACE 1
volatile uint8_t mSampBuff[SAMP_SRAM_DDR_DMA_PACKET_SIZE];   	// declare maximum size for DDR DMA
uint8_t mSampBuffOut[SAMP_SRAM_DDR_DMA_PACKET_SIZE];   			// use a circular buffer in SRAM
volatile uint8_t mArrayDdrBuffCount = 0;
volatile uint8_t mArrayDdrBuffIndex = 0;  						// will vary from 0 to mArrayDdrBuffCount-1
uint8_t mDdrBuffCount = 3;		// by default 3 DDR buff, this should be a parameter of sampling command
uint16_t mSramPacketSize;
uint32_t mSampCount;    // nb of compressed sample
uint8_t mLastSamp;
int8_t mSampRepet;


////////////////////////////////////////////////////////////////////

void User_HDR_Init(void){
	hsdb0.rvdev =  &rvdev;
	log_info("SDB OpenAMP-rpmsg channel creation\n");

	if (RPMSG_HDR_Init(&hsdb0) != RPMSG_HDR_OK) {
		log_err("RPMSG_HDR_Init HDR failed.\n");
		//Error_Handler();
	}

	if(RPMSG_HDR_RegisterCallback(&hsdb0, RPMSG_HDR_RXCPLT_CB_ID, SDB0_RxCpltCallback) != RPMSG_HDR_OK)
	{
		// Error_Handler();
	}
}

uint8_t char2hex(char asciiCar){
	uint8_t ret=0xFF;

	if( (asciiCar>='0') && (asciiCar<='9') ){
		ret = asciiCar-'0';
	}else if( (asciiCar>='A') && (asciiCar<='F') ){
		ret = asciiCar-'A'+10;
	}else if( (asciiCar>='a') && (asciiCar<='f') ){
		ret = asciiCar-'a'+10;
	}

	return ret;
}

// CallBack function which will be called when a DDR buffers is allocated by Linux.
void SDB0_RxCpltCallback(RPMSG_HDR_HandleTypeDef *huart)
{
	// copy received msg in a buffer
	SDB0ChannelRxSize = huart->RxXferSize < SDB_BUFFER_SIZE? huart->RxXferSize : SDB_BUFFER_SIZE-1;
	memcpy(SDB0ChannelBuffRx, huart->pRxBuffPtr, SDB0ChannelRxSize);
	//  SDB0ChannelBuffRx[SDB0ChannelRxSize] = 0;   // insure end of String

	// SDB0RxMsg = SET;
	struct DDR_SharedBuffer tmpSharedBuffer;

	if(DDR_readyToInit && !DDR_isInit){
		//struct DDR_SharedBuffer tmpSharedBuffer;
		tmpSharedBuffer.physAddr=0; tmpSharedBuffer.physSize=0;

		tmpSharedBuffer.B = SDB0ChannelBuffRx[0];
		tmpSharedBuffer.id = SDB0ChannelBuffRx[1]-48;

		tmpSharedBuffer.A = SDB0ChannelBuffRx[2];
		for(int i = 0; i<8; i++){
			tmpSharedBuffer.physAddr += ( char2hex(SDB0ChannelBuffRx[3+i])<<(28-i*4));
		}

		tmpSharedBuffer.L = SDB0ChannelBuffRx[11];
		for(int i = 0; i<8; i++){
			tmpSharedBuffer.physSize += ( char2hex(SDB0ChannelBuffRx[12+i])<<(28-i*4));
		}

		sharedMemBuff[tmpSharedBuffer.id] = tmpSharedBuffer;

		DDR_nbInitBuffer++;

		if(DDR_nbInitBuffer >= DDR_bufferToInit){
			DDR_isInit=1;
		}

	}else{
		SDB0RxMsg = SET;
	}

}


int User_SDB_write(uint8_t bufferid, uint8_t* msg, uint32_t size){

	if(size<=sharedMemBuff[bufferid].physSize){
		memcpy((char*)sharedMemBuff[bufferid].physAddr, msg, size);
	}

	sprintf(mSdbBuffTx, "B%dL%08x", bufferid, size);//SAMP_DDR_BUFFER_SIZE);
	RPMSG_HDR_Transmit(&hsdb0, (uint8_t*)mSdbBuffTx, strlen(mSdbBuffTx));

	return 0;
}


struct DDR_readBuffer User_SDB0_read(char* buffer){

	struct DDR_readBuffer readbuff;

	readbuff.id = SDB0ChannelBuffRx[1]-'0';
	for(int i = 0; i<8; i++){
		readbuff.size += ( char2hex(SDB0ChannelBuffRx[3+i])<<(28-i*4));
	}

	if(sizeof(buffer) >= readbuff.size){
		memcpy(buffer, (char*)sharedMemBuff[readbuff.id].physAddr, readbuff.size );
	}else{
		memcpy(buffer, (char*)sharedMemBuff[readbuff.id].physAddr, sizeof(buffer) );
	}

	return readbuff;
}

int User_SDB0_echo(void){
	uint8_t readId = 0;
	uint32_t readSize=0;

	static int echoBufferId =1;

	// get buffer informations
	readId = SDB0ChannelBuffRx[1]-'0';
	for(int i = 0; i<8; i++){
		readSize += ( char2hex(SDB0ChannelBuffRx[3+i])<<(28-i*4));
	}

	// copy read buffer to echo buffer
	if(readSize <= sharedMemBuff[echoBufferId].physSize){
		memcpy((char*)sharedMemBuff[echoBufferId].physAddr, (char*)sharedMemBuff[readId].physAddr , readSize);
	}else{
		memcpy((char*)sharedMemBuff[echoBufferId].physAddr, (char*)sharedMemBuff[readId].physAddr , sharedMemBuff[echoBufferId].physSize);
	}

	// send buffer to SDB
	sprintf(mSdbBuffTx, "B%dL%08x", echoBufferId, readSize);
	RPMSG_HDR_Transmit(&hsdb0, (uint8_t*)mSdbBuffTx, strlen(mSdbBuffTx));

	return 0;
}

int User_SDB0_bandwidth(void){
	static uint32_t buffersize=0;
	const uint8_t readId = 0, writeId = 1, ackId = 2;
	uint8_t receivedId=0;
	uint32_t readSize=0;

	// get buffer informations
	receivedId = SDB0ChannelBuffRx[1]-'0';
	for(int i = 0; i<8; i++){
		readSize += ( char2hex(SDB0ChannelBuffRx[3+i])<<(28-i*4));
	}

	if(receivedId == readId){
		// read buffer
		buffersize = readSize;
		if(buffersize <= sharedMemBuff[writeId].physSize){
			memcpy((char*)sharedMemBuff[ackId].physAddr, (char*)sharedMemBuff[receivedId].physAddr, buffersize);
		}else{
			memcpy((char*)sharedMemBuff[ackId].physAddr, (char*)sharedMemBuff[receivedId].physAddr, sharedMemBuff[writeId].physSize);
		}

		//send ack
		sprintf(mSdbBuffTx, "B%dL%08x", ackId, 11);
		RPMSG_HDR_Transmit(&hsdb0, (uint8_t*)mSdbBuffTx, strlen(mSdbBuffTx));
	}

	if(receivedId == ackId){
		// write buffer
		if(buffersize <= sharedMemBuff[writeId].physSize){
			memcpy((char*)sharedMemBuff[writeId].physAddr, (char*)sharedMemBuff[ackId].physAddr , buffersize);
		}else{
			memcpy((char*)sharedMemBuff[writeId].physAddr, (char*)sharedMemBuff[ackId].physAddr , sharedMemBuff[writeId].physSize);
		}

		// send buffer to SDB
		sprintf(mSdbBuffTx, "B%dL%08x", writeId, buffersize);
		RPMSG_HDR_Transmit(&hsdb0, (uint8_t*)mSdbBuffTx, strlen(mSdbBuffTx));
	}

	return 0;
}

int get_DDR_isInit(void){
	return DDR_isInit;
}

void set_DDR_isInit(int state){
	DDR_isInit = state;
}

void set_DDR_bufferToInit(int state){
	DDR_bufferToInit = state;
}

void set_DDR_readyToInit(int state){
	DDR_readyToInit = state;
}


