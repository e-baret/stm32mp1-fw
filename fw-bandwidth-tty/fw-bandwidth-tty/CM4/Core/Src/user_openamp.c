#include "user_openamp.h"

 bool OpenAmpIsInit = false;

 VIRT_UART_HandleTypeDef hvirtuart0;
 VIRT_UART_HandleTypeDef hvirtuart1;

 __IO FlagStatus VirtUart0RxMsg = RESET;
 uint8_t VirtUart0ChannelBuffRx[MAX_BUFFER_SIZE]={'\0'};
 uint8_t VirtUart0ChannelBuffTx[MAX_BUFFER_SIZE]={'\0'};
 uint16_t VirtUart0ChannelRxSize = 0;

 __IO FlagStatus VirtUart1RxMsg = RESET;
 uint8_t VirtUart1ChannelBuffRx[MAX_BUFFER_SIZE]={'\0'};
 uint8_t VirtUart1ChannelBuffTx[MAX_BUFFER_SIZE]={'\0'};
 uint16_t VirtUart1ChannelRxSize = 0;


void VIRT_UART0_RxCpltCallback(VIRT_UART_HandleTypeDef *huart)
{
	/* copy received msg in a variable to sent it back to master processor in main infinite loop*/
	VirtUart0ChannelRxSize = huart->RxXferSize < MAX_BUFFER_SIZE? huart->RxXferSize : MAX_BUFFER_SIZE-1;
	memcpy(VirtUart0ChannelBuffRx, huart->pRxBuffPtr, VirtUart0ChannelRxSize);
	VirtUart0RxMsg = SET;
}

void VIRT_UART1_RxCpltCallback(VIRT_UART_HandleTypeDef *huart)
{
	/* copy received msg in a variable to sent it back to master processor in main infinite loop*/
	VirtUart1ChannelRxSize = huart->RxXferSize < MAX_BUFFER_SIZE? huart->RxXferSize : MAX_BUFFER_SIZE-1;
	memcpy(VirtUart1ChannelBuffRx, huart->pRxBuffPtr, VirtUart1ChannelRxSize);
	VirtUart1RxMsg = SET;
}

void User_VIRT_UART_Init(void){
	log_info("Cortex-M4 boot successful with STM32Cube FW version: v%ld.%ld.%ld \r\n", ((HAL_GetHalVersion() >> 24) & 0x000000FF), ((HAL_GetHalVersion() >> 16) & 0x000000FF), ((HAL_GetHalVersion() >> 8) & 0x000000FF));
	log_info("Version : fw-rtos_CM4\r\n");
	/* Create Virtual UART device defined by a rpmsg channel attached to the remote device */

	log_info("Virtual UART0 OpenAMP-rpmsg channel creation\r\n");
	if (VIRT_UART_Init(&hvirtuart0) != VIRT_UART_OK) {
		log_err("VIRT_UART_Init UART0 failed.\r\n");
		//Error_Handler();
	}

	log_info("Virtual UART1 OpenAMP-rpmsg channel creation\r\n");
	if (VIRT_UART_Init(&hvirtuart1) != VIRT_UART_OK) {
		log_err("VIRT_UART_Init UART1 failed.\r\n");
		//Error_Handler();
	}

	/*Need to register callback for message reception by channels*/
	if(VIRT_UART_RegisterCallback(&hvirtuart0, VIRT_UART_RXCPLT_CB_ID, VIRT_UART0_RxCpltCallback) != VIRT_UART_OK){
		//Error_Handler();
	}

	if(VIRT_UART_RegisterCallback(&hvirtuart1, VIRT_UART_RXCPLT_CB_ID, VIRT_UART1_RxCpltCallback) != VIRT_UART_OK){
		//Error_Handler();
	}
}

void User_VIRT_UART0_Treatment(void){
	VirtUart0RxMsg = RESET;

	// send ACK to A7 when data are received
	sprintf((char *)VirtUart1ChannelBuffTx, "ACK\n\0");
	VIRT_UART_Transmit(&hvirtuart1, VirtUart1ChannelBuffTx, strlen((const char *)VirtUart1ChannelBuffTx));
}

void User_VIRT_UART1_Treatment(void){
	VirtUart1RxMsg = RESET;
	// send back databuffer to A7 when ACK is received on virtuart1
	VIRT_UART_Transmit(&hvirtuart0, VirtUart0ChannelBuffRx, VirtUart0ChannelRxSize);
}


