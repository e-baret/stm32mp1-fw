#include "user_openamp.h"

 bool OpenAmpIsInit = false;

 VIRT_UART_HandleTypeDef hvirtuart0;
 VIRT_UART_HandleTypeDef hvirtuart1;

 __IO FlagStatus VirtUart0RxMsg = RESET;
 uint8_t VirtUart0ChannelBuffRx[MAX_BUFFER_SIZE]={'\0'};
 uint8_t VirtUart0ChannelBuffTx[MAX_BUFFER_SIZE]={'\0'};
 uint16_t VirtUart0ChannelRxSize = 0;

 __IO FlagStatus VirtUart1RxMsg = RESET;
 uint8_t VirtUart1ChannelBuffRx[MAX_BUFFER_SIZE]={'\0'};
 uint8_t VirtUart1ChannelBuffTx[MAX_BUFFER_SIZE]={'\0'};
 uint16_t VirtUart1ChannelRxSize = 0;

//----------------------------------------------------------------------------//


 struct virt_uart virt_uart0 = {
   .rx_status = RESET,
   .rx_buffer = VirtUart0ChannelBuffRx,
   .rx_size = 0,
   .tx_status = RESET,
   .tx_buffer = VirtUart0ChannelBuffTx,
   .tx_size = 0,
   .cbk = VIRT_UART0_RxCpltCallback,
 };
 __IO uint16_t virt_uart0_expected_nbytes = 0;

 struct virt_uart virt_uart1 = {
   .rx_status = RESET,
   .rx_buffer = VirtUart1ChannelBuffRx,
   .rx_size = 0,
   .tx_status = RESET,
   .tx_buffer = VirtUart1ChannelBuffTx,
   .tx_size = 0,
   .cbk = VIRT_UART1_RxCpltCallback,
 };
 __IO uint16_t virt_uart1_expected_nbytes = 0;


//----------------------------------------------------------------------------//






void VIRT_UART0_RxCpltCallback(VIRT_UART_HandleTypeDef *huart)
{
	/* copy received msg in a variable to sent it back to master processor in main infinite loop*/

	VirtUart0ChannelRxSize = huart->RxXferSize < MAX_BUFFER_SIZE? huart->RxXferSize : MAX_BUFFER_SIZE-1;
	memcpy(VirtUart0ChannelBuffRx, huart->pRxBuffPtr, VirtUart0ChannelRxSize);
	VirtUart0RxMsg = SET;

	/*
	  uint16_t recv_size = huart->RxXferSize < MAX_BUFFER_SIZE? huart->RxXferSize : MAX_BUFFER_SIZE-1;

	  struct packet* in = (struct packet*) &huart->pRxBuffPtr[0];
	  if (in->preamble == PREAMBLE) {
	    in->preamble = 0;
	    virt_uart0_expected_nbytes = in->length;
	    //log_info("length: %d\n", virt_uart0_expected_nbytes);
	  }

	  virt_uart0.rx_size += recv_size;
	 // log_info("UART0: %d/%d\n", virt_uart0.rx_size, virt_uart0_expected_nbytes);
	  if (virt_uart0.rx_size >= virt_uart0_expected_nbytes) {
	    virt_uart0.rx_size = 0;
	    virt_uart0.tx_buffer[0] = virt_uart0_expected_nbytes & 0xff;
	    virt_uart0.tx_buffer[1] = (virt_uart0_expected_nbytes >> 8) & 0xff;
	   // log_info("UART0 resp: %d\n", virt_uart0_expected_nbytes);
	    virt_uart0_expected_nbytes = 0;
	    virt_uart0.tx_size = 2;
	    virt_uart0.tx_status = SET;
	    // huart->RxXferSize = 0;
	  }
	*/

}

void VIRT_UART1_RxCpltCallback(VIRT_UART_HandleTypeDef *huart)
{
	/* copy received msg in a variable to sent it back to master processor in main infinite loop*/
	//log_info("Msg received on VIRTUAL UART1 channel:  %s \n\r", (char *) huart->pRxBuffPtr);
	VirtUart1ChannelRxSize = huart->RxXferSize < MAX_BUFFER_SIZE? huart->RxXferSize : MAX_BUFFER_SIZE-1;
	memcpy(VirtUart1ChannelBuffRx, huart->pRxBuffPtr, VirtUart1ChannelRxSize);
	VirtUart1RxMsg = SET;
}

void User_VIRT_UART_Init(void){
	log_info("Cortex-M4 boot successful with STM32Cube FW version: v%ld.%ld.%ld \r\n", ((HAL_GetHalVersion() >> 24) & 0x000000FF), ((HAL_GetHalVersion() >> 16) & 0x000000FF), ((HAL_GetHalVersion() >> 8) & 0x000000FF));
	log_info("Version : fw-rtos_CM4\r\n");
	/* Create Virtual UART device defined by a rpmsg channel attached to the remote device */

	log_info("Virtual UART0 OpenAMP-rpmsg channel creation\r\n");
	if (VIRT_UART_Init(&hvirtuart0) != VIRT_UART_OK) {
		log_err("VIRT_UART_Init UART0 failed.\r\n");
		//Error_Handler();
	}

	log_info("Virtual UART1 OpenAMP-rpmsg channel creation\r\n");
	if (VIRT_UART_Init(&hvirtuart1) != VIRT_UART_OK) {
		log_err("VIRT_UART_Init UART1 failed.\r\n");
		//Error_Handler();
	}

	/*Need to register callback for message reception by channels*/

	if(VIRT_UART_RegisterCallback(&hvirtuart0, VIRT_UART_RXCPLT_CB_ID, VIRT_UART0_RxCpltCallback) != VIRT_UART_OK){
		//Error_Handler();
	}

	if(VIRT_UART_RegisterCallback(&hvirtuart1, VIRT_UART_RXCPLT_CB_ID, VIRT_UART1_RxCpltCallback) != VIRT_UART_OK){
		//Error_Handler();
	}
}

void User_VIRT_UART0_Treatment(void){
	VirtUart0RxMsg = RESET;
	log_info("Msg received on VIRTUAL UART0 channel:  %s \n\r", VirtUart0ChannelBuffRx);
	VIRT_UART_Transmit(&hvirtuart0, VirtUart0ChannelBuffRx, VirtUart0ChannelRxSize);
//	memset(VirtUart0ChannelBuffRx, '\0', strlen((char*) VirtUart0ChannelBuffRx));
}

void User_VIRT_UART1_Treatment(void){
	VirtUart1RxMsg = RESET;
	log_info("Msg received on VIRTUAL UART1 channel:  %s \n\r", VirtUart1ChannelBuffRx);
	if (!strncmp((char *)VirtUart1ChannelBuffRx, MSG_TIME, strlen(MSG_TIME)))
	{
		//sprintf((char *)BuffTx, "m4: time elapsed : %ld:%d \n",  (countSec/60),(int)(countSec%60) );
		sprintf((char *)VirtUart1ChannelBuffTx, "[M4] time elapsed : %ld:%02ld.%03d \n", (xTaskGetTickCount()/60000),(xTaskGetTickCount()/1000)%60, (int)(xTaskGetTickCount()%1000) );
		VIRT_UART_Transmit(&hvirtuart1, VirtUart1ChannelBuffTx, strlen((const char *)VirtUart1ChannelBuffTx));
	}
	else if (!strncmp((char *)VirtUart1ChannelBuffRx, MSG_LED_ON, strlen(MSG_LED_ON)))
	{
		sprintf((char *)VirtUart1ChannelBuffTx, "[M4] led on \n");
		VIRT_UART_Transmit(&hvirtuart1, VirtUart1ChannelBuffTx, strlen((const char *)VirtUart1ChannelBuffTx));
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_13, 0);
	}
	else if (!strncmp((char *)VirtUart1ChannelBuffRx, MSG_LED_OFF, strlen(MSG_LED_OFF)))
	{
		sprintf((char *)VirtUart1ChannelBuffTx, "[M4] led off \n");
		VIRT_UART_Transmit(&hvirtuart1, VirtUart1ChannelBuffTx, strlen((const char *)VirtUart1ChannelBuffTx));
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_13, 1);
	}
	else if (!strncmp((char *)VirtUart1ChannelBuffRx, MSG_DELAY, strlen(MSG_DELAY)))
	{
		osDelay(20000); // 20 sec
		sprintf((char *)VirtUart1ChannelBuffTx, "[M4] delay return \n");
		VIRT_UART_Transmit(&hvirtuart1, VirtUart1ChannelBuffTx, strlen((const char *)VirtUart1ChannelBuffTx));
	}
	else if (!strncmp((char *)VirtUart1ChannelBuffRx, MSG_CLR_BUF, strlen(MSG_CLR_BUF)))
	{
			sprintf((char *)VirtUart1ChannelBuffTx, "[M4] clear buffers \n");
			VIRT_UART_Transmit(&hvirtuart1, VirtUart1ChannelBuffTx, strlen((const char *)VirtUart1ChannelBuffTx));
			memset(hvirtuart0.pRxBuffPtr, '\0', strlen((char*) MAX_BUFFER_SIZE));
			memset(hvirtuart1.pRxBuffPtr, '\0', strlen((char*) MAX_BUFFER_SIZE));
			memset(VirtUart0ChannelBuffRx, '\0', strlen((char*) VirtUart0ChannelBuffRx));
			memset(VirtUart1ChannelBuffRx, '\0', strlen((char*) VirtUart1ChannelBuffRx));
			memset(VirtUart0ChannelBuffTx, '\0', strlen((char*) VirtUart0ChannelBuffTx));
			memset(VirtUart1ChannelBuffTx, '\0', strlen((char*) VirtUart1ChannelBuffTx));
	}
	else
	{
		sprintf((char *)VirtUart1ChannelBuffTx, "[M4] echo : %s", VirtUart1ChannelBuffRx);
		//sprintf((char *)BuffTx, "[m4] unknown %s\n", VirtUart1ChannelBuffRx);
		VIRT_UART_Transmit(&hvirtuart1, VirtUart1ChannelBuffTx, strlen((const char *)VirtUart1ChannelBuffTx));
	}

}


